sap.ui.define([
	"hcc/training/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("hcc.training.controller.Anlage", {
		
		onSave: function() {
			var oView = this.getView();
			var oModel = oView.getModel();
			var sPath = "/ZUI5_TEST_00Set";
			var oData = {
				UniqueId: oView.byId("idInputUniqueId").getValue(),
				Ui5Txt01: oView.byId("idInputUi5Txt01").getValue(),
				Ui5Txt02: oView.byId("idInputUi5Txt02").getValue()
			};
			oModel.create(sPath, oData);
		}
		
	});
});